clear all
clc

Data = load('2018-06-19 091637.txt');
Data1 = load('2018-11-03 152600.txt');
Data2 = load('Chamber9_27b.txt');

%Grabbing Temperature Value-------------------------------------
TurboChamber = Data(:,3);
TurboTube = Data1(:,3);
TurboChamberLoad = Data2(:,3);

%Load time------------------------------------------------------
fid = fopen('2017-09-30 010847.txt');
date = textscan(fid, '%q %q %*[^\n]');
sts = fclose(fid);
DateTime = strcat(date{1}, {' '},date{2});
T = datenum(DateTime, 'yyyy-mm-dd HH:MM:SS');
time = datevec(T, 'HH:MM:SS');
%---------------------------------------------------------------

%Load time------------------------------------------------------
fid1 = fopen('2017-09-28 133308.txt');
date1 = textscan(fid1, '%q %q %*[^\n]');
sts1 = fclose(fid1);
DateTime1 = strcat(date1{1}, {' '},date1{2});
T1 = datenum(DateTime1, 'yyyy-mm-dd HH:MM:SS');
time1 = datevec(T1, 'HH:MM:SS');
%---------------------------------------------------------------

%Load time------------------------------------------------------
fid2 = fopen('Chamber9_27b.txt');
date2 = textscan(fid2, '%q %q %*[^\n]');
sts2 = fclose(fid2);
DateTime2 = strcat(date2{1}, {' '},date2{2});
T2 = datenum(DateTime2, 'yyyy-mm-dd HH:MM:SS');
time2 = datevec(T2, 'HH:MM:SS');
%---------------------------------------------------------------

for c = 1:length(T)
    C(c)=T(c)-T(1);
end

for c = 1:length(T1)
    C1(c)=T1(c)-T1(1);
end

for c = 1:length(T2)
    C2(c)=T2(c)-T2(1);
end

[M,I] = max(TurboChamber);
k = length(TurboTube);

%-------------------------------------
for i = 10000:length(TurboTube)
    if TurboTube(i) <= M
        Tube = TurboTube(i:k);
        Time = C1(i:k);
        break;
    end
end

for c = 1:length(Time)
    R(c) = Time(c)-Time(1);
end
%-------------------------------------

k = length(TurboChamberLoad);
%-------------------------------------
for i = 30:length(TurboChamberLoad)
    if TurboChamberLoad(i) <= M
        Load = TurboChamberLoad(i:k);
        Time = C2(i:k);
        break;
    end
end

for c = 1:length(Time)
    D(c) = Time(c)-Time(1);
end
%-------------------------------------

k = length(TurboChamber);
Chamber = TurboChamber(I:k);
Time = C(I:k);
for c = 1:length(Time)
    B(c) = Time(c)-Time(1);
end

% VTube = (Tube * (3.3/1023.0)) * 2.722;
% TubePressure = 10.^(VTube - 5.5);
% VChamber = (Chamber * (3.3/1023.0)) * 2.722;
% TubeChamber = 10.^(VChamber - 5.5);

figure
% PlotTube = semilogy(C,TubePressure);
hold on
PlotChamber = semilogy(B,Chamber);
PlotChamberLoad = semilogy(D,Load);
PlotTube = semilogy(R,Tube);
legend([PlotChamber PlotChamberLoad PlotTube],'Chamber','Chamber With Load','Tube');
% legend([PlotTube PlotChamber PlotTurbo], 'Tube', 'Chamber','Turbo Pump');
title('Pressure');
grid on
datetick('x','HH:MM');
xlabel('Time (Hours)');
ylabel('mbar');
%ylim([0 0.01]);