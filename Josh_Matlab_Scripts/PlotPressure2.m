clear all
clc

Data = load('2018-06-19 091637.txt');

%Grabbing Temperature Value-------------------------------------
Turbo = Data(:,3);
TubeH = Data(:,4);
Tube = Data(:,5);
Switch = Data(:,6);
Chamber = Data(:,7);

%Load time------------------------------------------------------
fid = fopen('2017-10-10 005345.txt');
date = textscan(fid, '%q %q %*[^\n]');
sts = fclose(fid);
DateTime = strcat(date{1}, {' '},date{2});
T = datenum(DateTime, 'yyyy-mm-dd HH:MM:SS');
time = datevec(T, 'HH:MM:SS');
%---------------------------------------------------------------

for c = 1:length(T)
    C(c)=T(c)-T(1);
end

VTube = (Tube * (3.3/1023.0)) * 2.722;
TubePressure = 10.^(VTube - 5.5);

VChamber = (Chamber * (3.3/1023.0)) * 2.722;
TubeChamber = 10.^(VChamber - 5.5);

VSwitch = (Switch * (3.3/1023.0)) * 2.722;
SwitchPressure = 10.^(VSwitch - 5.5);

figure
PlotTurbo = semilogy(C,Turbo);
hold on
PlotTubeH = semilogy(C,TubeH);
PlotTube = semilogy(C,TubePressure);
PlotChamber = semilogy(C,TubeChamber);
PlotSwitch = semilogy(C,SwitchPressure);

legend([PlotTurbo PlotTubeH PlotTube PlotChamber PlotSwitch],'Turbo Pump','Tube Nice Gauge','Tube','Chamber','Heat Switch');
title('Pressure');
grid on
datetick('x','HH:MM');
xlabel('Time (Hours)');
ylabel('mbar');