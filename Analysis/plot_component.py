###IMPORTS###
import sys

import numpy as np
import pandas as pd

import file

import matplotlib as mpl
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 18})

#bmh
plt.style.use('seaborn-whitegrid')
#classic
plt.style.use('seaborn-whitegrid')
#dark_background
plt.style.use('seaborn-whitegrid')
#fivethirtyeight
plt.style.use('seaborn-whitegrid')
#ggplot
plt.style.use('seaborn-whitegrid')
#grayscale
plt.style.use('seaborn-whitegrid')
#seaborn-bright
plt.style.use('seaborn-whitegrid')
#seaborn-colorblind
plt.style.use('seaborn-whitegrid')
#seaborn-dark
plt.style.use('seaborn-whitegrid')
#seaborn-dark-palette
plt.style.use('seaborn-whitegrid')
##seaborn-darkgrid
#plt.style.use('seaborn-darkgrid')
##seaborn-deep
#plt.style.use('seaborn-deep')
##seaborn-muted
#plt.style.use('seaborn-muted')
##seaborn-notebook
#plt.style.use('seaborn-notebook')
##seaborn-paper
#plt.style.use('seaborn-paper')
##seaborn-pastel
#plt.style.use('seaborn-pastel')
##seaborn-poster
#plt.style.use('seaborn-poster')
##seaborn-talk
#plt.style.use('seaborn-talk')
##seaborn-ticks
#plt.style.use('seaborn-ticks')
##seaborn-white
#plt.style.use('seaborn-white')
##seaborn-whitegrid
#plt.style.use('seaborn-whitegrid')

import seaborn as sns
import DataSet_Abstract as ADS
import DataSet_Cycle as CCDS
import DataSet_Uniform as CUDS
import Groups
####################
#TODO::Fix the plot limits
#TODO::Are there capacitors of multiple types in the Capacitor files
####################
def plot_BP_Resistors(abstract_DataSet):
    print("top of BP resistor plot function in plot_components")

    if isinstance(abstract_DataSet,CCDS.DataSet_Cycle):
        print("a cycle was passed in")
        df = abstract_DataSet.DataFrame             #ADS.DataSet_Abstract.DataFrame
        cycleDict = abstract_DataSet.DataSetDict    #ADS.DataSet_Abstract.DataSetDict

        fig, ((ax1), (ax2)) = plt.subplots(nrows=2, ncols=1, sharex=True)
        cycleTime = list(cycleDict["CorrectedTimes"])

        tempList = list(cycleDict["resistBP1List"])
        for item in tempList:
            df.iloc[:, item].plot(x=cycleTime, y=df.iloc[:, item], ylim=(0, 200), ax=ax1)
        ax1.set_title("BackPlane 1 Resistors")

        tempList = list(cycleDict["resistBP2List"])
        for item in tempList:
            df.iloc[:, item].plot(x=cycleTime, y=df.iloc[:, item], ylim=(0, 200), ax=ax2)
        ax2.set_title("Backplane 2 Resistors")

        cycleNumber = cycleDict.get('Cycle')
        fig.suptitle('Cycle ' + str(cycleNumber) + ' Resistors', fontsize='large')

    if isinstance(ADS.DataSet_Abstract, CUDS.DataSet_Uniform):
        print("a non-cycle constant was passed in")



# def plot_BP_Resistors(CycleDataSet):
#     df = CycleDataSet.cycleDataFrame
#     cycleDict = CycleDataSet.cycleInfo
#
#     fig,((ax1),(ax2)) = plt.subplots(nrows=2,ncols=1,sharex=True)
#     cycleTime = list(cycleDict["CorrectedTimes"])
#
#     tempList = list(cycleDict["resistBP1List"])
#     for item in tempList:
#         df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(-1.75,40),ax=ax1)
#     ax1.set_title("BackPlane 1 Resistors")
#
#     tempList = list(cycleDict["resistBP2List"])
#     for item in tempList:
#         df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,57),ax=ax2)
#     ax2.set_title("Backplane 2 Resistors")
#
#     cycleNumber = cycleDict.get('Cycle')
#     fig.suptitle('Cycle '+str(cycleNumber)+' Resistors',fontsize='large')


def plot_MUX_Resistors(CycleDataSet):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict

    fig, ((ax1)) = plt.subplots(nrows=1,ncols=1,sharex=True)
    cycleTime = list(cycleDict["CorrectedTimes"])
    cycleNumber = cycleDict.get('Cycle')

    color = 'tab:blue'
    tempList = list(cycleDict["resistMUXList"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(9,123),ax=ax1)
    ax1.set_ylabel('Resistance', color=color, fontsize=20)
    ax1.set_title('Cycle '+str(cycleNumber)+' MUX Resistors')
    ax1.set_xlabel('Time', fontsize=18)

    color = 'tab:red'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Temperature', color=color, fontsize=20)  # we already handled the x-label with ax1
    ax2.tick_params(axis='y', labelcolor=color)
    tempString = df.columns.get_loc("C3")
    df.iloc[:, tempString].plot(x=cycleTime, y=df.iloc[:, item], ylim=(9, 310), ax=ax2, color=color, linewidth=3.3)
    ax2.grid(False)



def plot_ALL_Resistors(CycleDataSet):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict

    fig, ((ax1),(ax2),(ax3)) = plt.subplots(nrows=3,ncols=1,sharex=True)
    cycleTime = list(cycleDict["CorrectedTimes"])

    tempList = list(cycleDict["resistBP1List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(-1.75,40),ax=ax1)
    ax1.set_title("BackPlane 1 Resistors")

    tempList = list(cycleDict["resistBP2List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(-1.75,10),ax=ax2)
    ax2.set_title("Backplane 2 Resistors")

    tempList = list(cycleDict["resistMUXList"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(15,65),ax=ax3)
    ax3.set_title("MUX Resistors")

    cycleNumber = cycleDict.get('Cycle')
    fig.suptitle('Cycle '+str(cycleNumber)+' Resistors',fontsize='large')


def plot_Capacitors(CycleDataSet):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict

    fig,((ax1),(ax2)) = plt.subplots(nrows=2,ncols=1,sharex=True)
    cycleTime = list(cycleDict["CorrectedTimes"])

    tempList = list(cycleDict["capBP1List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,7200),ax=ax1)
    ax1.set_title("BackPlane 1 Capacitors")

    tempList = list(cycleDict["capBP2List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,7100),ax=ax2)
    ax2.set_title("Backplane 2 Capacitors")

    cycleNumber = cycleDict.get('Cycle')
    fig.suptitle('Cycle '+str(cycleNumber)+' Capacitors',fontsize='large')


def plot_SMT_Capacitors(CycleDataSet):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict

    fig,((ax1),(ax2)) = plt.subplots(nrows=2,ncols=1,sharex=True)
    cycleTime = list(cycleDict["CorrectedTimes"])

    tempList = list(cycleDict["capBP1List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,65),ax=ax1)
    ax1.set_title("BackPlane 1 Capacitors")

    tempList = list(cycleDict["capBP2List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,65),ax=ax2)
    ax2.set_title("Backplane 2 Capacitors")

    cycleNumber = cycleDict.get('Cycle')
    fig.suptitle('Cycle '+str(cycleNumber)+' Capacitors',fontsize='large')


def plot_Cycle(CycleDataSet):
    df = CycleDataSet.DataFrame
    Info500 = CycleDataSet.DataSetDict
    fig, ((ax1,ax2,ax3,ax4,ax5)) = plt.subplots(nrows=5,ncols=1,sharex=True)#,sharey=True,figsize=(10,5))
    cycleTime = list(Info500["CorrectedTimes"])

    tempList = list(Info500["resistBP1List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item], ylim=(0,90), ax=ax1)
    ax1 = plt.gca()
    #ax1.set_title("Resistors: BackPlane 1")

    tempList = list(Info500["resistBP2List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item], ylim=(0,1.6), ax=ax2)
    ax2 = plt.gca()
    #ax2.set_title("Resistors: BackPlane 2")

    tempList = list(Info500["capBP1List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ax=ax3)
    ax3 = plt.gca()
    #ax3.set_title("Capacitors: BackPlane 1")

    tempList = list(Info500["capBP2List"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ax=ax4)
    ax4 = plt.gca()
    #ax4.set_title("Capacitors: BackPlane 2")

    tempList = list(Info500["resistMUXList"])
    for item in tempList:
        df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ax=ax5)
    ax5 = plt.gca()
    #ax5.set_title("Resistors: MUX")


def plot_Component(CycleDataSet, itemString):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict
    item = df.columns.get_loc(str(itemString))

    fig,(ax1) = plt.subplots(nrows=1,ncols=1)
    cycleTime = list(cycleDict["CorrectedTimes"])


    color = 'tab:blue'
    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2,6.2),ax=ax1,color=color, linewidth=3.3)
    ax1.set_xlabel('Time', fontsize=18)
    ax1.set_ylabel('Resistance', color=color, fontsize=20)


    color = 'tab:red'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Temperature', color=color, fontsize=20)  # we already handled the x-label with ax1
    ax2.tick_params(axis='y', labelcolor=color)
    tempString = df.columns.get_loc("C3")
    df.iloc[:, tempString].plot(x=cycleTime, y=df.iloc[:, item], ylim=(0,310), ax=ax2, color=color, linewidth=3.3)
    ax2.grid(False)

    #ax.set_title(str(itemString))

    cycleNumber = cycleDict.get('Cycle')
    fig.suptitle('Cycle '+str(cycleNumber)+": "+str(itemString),fontsize='large')
    #fig.suptitle('CVBGA97: Cycle 500', fontsize=21)


    # leg = fig.legend()
    # # get the lines and texts inside legend box
    # leg_lines = leg.get_lines()
    # leg_texts = leg.get_texts()
    # # bulk-set the properties of all lines and texts
    # plt.setp(leg_lines, linewidth=4)
    # plt.setp(leg_texts, fontsize='x-large')

    fig.tight_layout()  # otherwise the right y-label is slightly clipped



def plot_Components(CycleDataSet, itemList):
    df = CycleDataSet.DataFrame
    cycleDict = CycleDataSet.DataSetDict

    fig,(ax) = plt.subplots(nrows=1,ncols=1,sharex=True)
    cycleTime = list(cycleDict["CorrectedTimes"])

    title = " "
    for itemString in itemList:
        itemColumn = df.columns.get_loc("R"+str(itemString)+":BP2")
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(1.75,57),ax=ax)
        title = title+str(itemString)+", "
    else:
        title = title[:-2]
    #ax.set_title(str(title))
    plt.legend()

    cycleNumber = cycleDict.get('Cycle')
    fig.suptitle('Cycle '+str(cycleNumber)+': Selected Components',fontsize='large')


def plot_Group(CycleDataSet, Component_Group):
    df = CycleDataSet.DataFrame
    Info500 = CycleDataSet.DataSetDict
    fig, ((ax1,ax2)) = plt.subplots(nrows=2,ncols=1,sharex=True)#,sharey=True,figsize=(10,5))
    cycleTime = list(Info500["CorrectedTimes"])

    itemList = list(Component_Group)
    resistorList = [element for element in itemList if "R" in element]
    capacitorList  = [element for element in itemList if "C" in element]

    for itemString in resistorList:
        itemColumn = df.columns.get_loc(str(itemString))
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(-.1,6.1),ax=ax1)
    ax1.set_title("Resistors: Group")
    ax1 = plt.gca()
    for itemString in capacitorList:
        itemColumn = df.columns.get_loc(str(itemString))
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(50,60),ax=ax2)
    ax2.set_title("Capacitors: Group")
    ax2 = plt.gca()

    cycleNumber = Info500.get('Cycle')
    fig.suptitle('Cycle ' + str(cycleNumber) + ': Selected Components', fontsize='large')


def plot_resistor_Group(CycleDataSet, Component_Group):
    # df = CycleDataSet.DataFrame
    # cycleDict = CycleDataSet.DataSetDict
    # item = df.columns.get_loc(str(itemString))
    #
    # fig, (ax1) = plt.subplots(nrows=1, ncols=1)
    # cycleTime = list(cycleDict["CorrectedTimes"])
    #
    # color = 'tab:blue'
    # df.iloc[:, item].plot(x=cycleTime, y=df.iloc[:, item], ylim=(2, 6.2), ax=ax1, color=color, linewidth=3.3)
    # ax1.set_xlabel('Time', fontsize=18)
    # ax1.set_ylabel('Resistance', color=color, fontsize=20)
    #
    # color = 'tab:red'
    # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    # ax2.set_ylabel('Temperature', color=color, fontsize=20)  # we already handled the x-label with ax1
    # ax2.tick_params(axis='y', labelcolor=color)
    # tempString = df.columns.get_loc("C3")
    # df.iloc[:, tempString].plot(x=cycleTime, y=df.iloc[:, item], ylim=(0, 310), ax=ax2, color=color, linewidth=3.3)
    # ax2.grid(False)


    #####################################################

    df = CycleDataSet.DataFrame
    Info500 = CycleDataSet.DataSetDict
    fig, ((ax1)) = plt.subplots(nrows=1,ncols=1,sharex=True)#,sharey=True,figsize=(10,5))
    cycleTime = list(Info500["CorrectedTimes"])

    itemList = list(Component_Group)
    resistorList = [element for element in itemList if "R" in element]

    for itemString in resistorList:
        itemColumn = df.columns.get_loc(str(itemString))
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(-2,6),ax=ax1) #-.1,2
    ax1.set_xlabel('Time', fontsize=18)
    ax1.set_ylabel('Resistance: Group', fontsize=20)

    color = 'tab:red'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Temperature', color=color, fontsize=20)  # we already handled the x-label with ax1
    ax2.tick_params(axis='y', labelcolor=color)
    tempString = df.columns.get_loc("C3")
    item = df.columns.get_loc(str(itemString))
    df.iloc[:, tempString].plot(x=cycleTime, y=df.iloc[:, item], ylim=(0, 310), ax=ax2, color=color, linewidth=3.3)
    ax2.grid(False)

    cycleNumber = Info500.get('Cycle')
    fig.suptitle('Cycle ' + str(cycleNumber) + ': Selected Components', fontsize='large')


def plot_capacitor_Group(CycleDataSet, Component_Group):
    df = CycleDataSet.DataFrame
    Info500 = CycleDataSet.DataSetDict
    fig, ((ax)) = plt.subplots(nrows=1,ncols=1,sharex=True)#,sharey=True,figsize=(10,5))
    cycleTime = list(Info500["CorrectedTimes"])

    itemList = list(Component_Group)
    capacitorList = [element for element in itemList if "C" in element]

    for itemString in capacitorList:
        itemColumn = df.columns.get_loc(str(itemString))
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(0,10000),ax=ax) #-.1,2
    ax.set_title("Capacitors: Group")
    ax = plt.gca()

    cycleNumber = Info500.get('Cycle')
    fig.suptitle('Cycle ' + str(cycleNumber) + ': Selected Components', fontsize='large')


def plot_mux_Group(CycleDataSet, Component_Group):
    df = CycleDataSet.DataFrame
    Info500 = CycleDataSet.DataSetDict
    fig, ((ax)) = plt.subplots(nrows=1,ncols=1,sharex=True)#,sharey=True,figsize=(10,5))
    cycleTime = list(Info500["CorrectedTimes"])

    itemList = list(Component_Group)
    muxList = [element for element in itemList if "MUX" in element]

    for itemString in muxList:
        itemColumn = df.columns.get_loc(str(itemString))
        df.iloc[:,itemColumn].plot(x=cycleTime,y=df.iloc[:,itemColumn],ylim=(0,55),ax=ax) #-.1,2
    ax.set_title("MUX: Group")
    ax = plt.gca()

    cycleNumber = Info500.get('Cycle')
    fig.suptitle('Cycle ' + str(cycleNumber) + ': Selected Components', fontsize='large')

######################################################################################

def plot_Difference(Minuend, Subtrahend, itemString):
    df_minuend = Minuend.DataFrame
    cycleDict_minuend = Minuend.DataSetDict
    item_minuend = df_minuend.columns.get_loc(str(itemString))

    df_subtrahend = Subtrahend.DataFrame
    cycleDict_subtrahend = Subtrahend.DataSetDict
    item = df_subtrahend.columns.get_loc(str(itemString))


    fig,(ax1) = plt.subplots(nrows=1,ncols=1)
    cycleTime = list(cycleDict_minuend["CorrectedTimes"])

    #df_minuend.subtract(df_subtrahend, axis=itemString, fill_value=0)
    ##df_difference = df.sort_values(by=['name', 'timestamp'])
    ##df_['time_diff'] = df.groupby('name')['timestamp'].diff()
    #df_minuend['difference'] = df_minuend[str(itemString)] - df_subtrahend[str(itemString)]
    df_minuend['difference'] = 0
    print(df_minuend)
    print("after subtracting")
    df_minuend['difference'] = df_minuend[str(itemString)] - df_subtrahend[str(itemString)]
    print(df_minuend)
    diff_string = "difference"
    difference = df_minuend.columns.get_loc(str(diff_string))


    color = 'tab:blue'
    #df_minuend.iloc[:,item].plot(x=cycleTime,y=df_minuend.iloc[:,item],ylim=(1.65,15),ax=ax1,color=color, linewidth=3.3)
    df_minuend.iloc[:, difference].plot(x=cycleTime, y=df_minuend.iloc[:, difference], ylim=(-150, 150), ax=ax1, color=color,
                                  linewidth=3.3)
    ax1.set_xlabel('Time', fontsize=18)
    ax1.set_ylabel('Resistance', color=color, fontsize=20)

    color = 'tab:red'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Temperature', color=color, fontsize=20)  # we already handled the x-label with ax1
    ax2.tick_params(axis='y', labelcolor=color)
    tempString = df_subtrahend.columns.get_loc("C3")
    df_subtrahend.iloc[:, tempString].plot(x=cycleTime, y=df_subtrahend.iloc[:, item], ylim=(0,310), ax=ax2, color=color, linewidth=3.3)
    ax2.grid(False)

    cycleNumberMinuend = cycleDict_minuend.get('Cycle')
    cycleNumberSubtrahend = cycleDict_subtrahend.get('Cycle')
    fig.suptitle(itemString + ': Cycle ' + str(cycleNumberSubtrahend)+' versus ' + str(cycleNumberMinuend), fontsize=21)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped

######################################################################################


#plt.tight_layout()

##resistor 223 is 470 column in array
#####################################

#fig, ((ax1),(ax2),(ax3),(ax4),(ax5),(ax6)) = plt.subplots(nrows=6,ncols=1, sharex=True)
#
#Cycle500 = file.CycleDataSet(const.FNAME340, 359)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax1,label='Bhutan')
#ax1.set_title("Cycle 359 Resistor 223: BackPlane 2")
#
#Cycle500 = file.CycleDataSet(const.FNAME360, 360)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax2,label='Bhutan')
#ax2.set_title("Cycle 360 Resistor 223: BackPlane 2")
#
#Cycle500 = file.CycleDataSet(const.FNAME379, 390)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax3,label='Bhutan')
#ax3.set_title("Cycle 390 Resistor 223: BackPlane 2")
#
#Cycle500 = file.CycleDataSet(const.FNAME419, 429)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax4,label='Bhutan')
#ax4.set_title("Cycle 429 Resistor 223: BackPlane 2")
#
#Cycle500 = file.CycleDataSet(const.FNAME446, 461)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax5,label='Bhutan')
#ax5.set_title("Cycle 461 Resistor 223: BackPlane 2")
#
#Cycle500 = file.CycleDataSet(const.FNAME499, 500)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list()
#tempList.append(470)
#tempList.append(471)
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,95),ax=ax6,label='Bhutan')
#ax6.set_title("Cycle 500 Resistor 223: BackPlane 2")
######################################################################
#fig, ((ax1)) = plt.subplots(nrows=1,ncols=1, sharex=True)
#
#Cycle500 = file.CycleDataSet(const.FNAME340, 359)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(Info500["resistBP2List"])
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.48,5.1),ax=ax1,label='Bhutan')
#ax1.set_title("Cycle 359 Resistor 223: BackPlane 2")
####################################################

#ax = plt.axes()
#ax.scatter(x, y)
#ax.set(ylim=(2.5,6.5),xlabel='x',ylabel='y',title='A Simple Plot')
#ax.xaxis.set_major_locator(ticker.AutoLocator())

#ax = plt.axes()
#ax.errorbar(x, y, yerr=dy, fmt='.k')
#ax.set(ylim=(2.5,6.5),xlabel='x',ylabel='y',title='A Simple Plot')
#ax.xaxis.set_major_locator(ticker.AutoLocator())

#ax = plt.axes()
#ax.hist2d(x, y, bins=10)
#ax.set(ylim=(2.5,6.5),xlabel='x',ylabel='y',title='A Simple Plot')
#ax.xaxis.set_major_locator(ticker.AutoLocator())




#TODO::redo x axis when its time so that it shows zeroed timescale

#plt.hist(resistBP1openList,histtype='step')

####################################################################
#plt.axis('tight');
#plt.axis('equal');


#plt.plot(x, np.sin(x - 0), color='blue')        # specify color by name
#plt.plot(x, np.sin(x - 1), color='g')           # short color code (rgbcmyk)
#plt.plot(x, np.sin(x - 2), color='0.75')        # Grayscale between 0 and 1
#plt.plot(x, np.sin(x - 3), color='#FFDD44')     # Hex code (RRGGBB from 00 to FF)
#plt.plot(x, np.sin(x - 4), color=(1.0,0.2,0.3)) # RGB tuple, values 0 to 1
#plt.plot(x, np.sin(x - 5), color='chartreuse'); # all HTML color names supported
##If no color is specified, Matplotlib will
##automatically cycle through a set of default
##colors for multiple lines.

##Similarly, the line style can be adjusted using the
##linestyle keyword:
#plt.plot(x, x + 0, linestyle='solid')
#plt.plot(x, x + 1, linestyle='dashed')
#plt.plot(x, x + 2, linestyle='dashdot')
#plt.plot(x, x + 3, linestyle='dotted');

## For short, you can use the following codes:
#plt.plot(x, x + 4, linestyle='-')  # solid
#plt.plot(x, x + 5, linestyle='--') # dashed
#plt.plot(x, x + 6, linestyle='-.') # dashdot
#plt.plot(x, x + 7, linestyle=':');  # dotted

##Matplotlib does a decent job of choosing default axes limits
##for your plot, but sometimes it's nice to have finer
##control. The most basic way to adjust axis limits is
##to use the plt.xlim() and plt.ylim() methods:
#plt.plot(x, np.sin(x))
#plt.xlim(-1, 11)
#plt.ylim(-1.5, 1.5);

##If for some reason you'd like either axis to be
##displayed in reverse, you can simply reverse the
##order of the arguments:
#plt.plot(x, np.sin(x))
#plt.xlim(10, 0)
#plt.ylim(1.2, -1.2);

##A useful related method is plt.axis(). The plt.axis()
##method allows you to set the x and y limits with a
##single call, by passing a list which specifies
##[xmin, xmax, ymin, ymax]:
#plt.plot(x, np.sin(x))
#plt.axis([-1, 11, -1.5, 1.5]);
##The plt.axis() method goes even beyond this,
##allowing you to do things like automatically
##tighten the bounds around the current plot:
#plt.plot(x, np.sin(x))
#plt.axis('tight');
##It allows even higher-level specifications, such as
##ensuring an equal aspect ratio so that on your
##screen, one unit in x is equal to one unit in y:
#plt.plot(x, np.sin(x))
#plt.axis('equal');

##Titles and axis labels are the simplest such
##labels—there are methods that can be used to
##quickly set them:
#plt.plot(x, np.sin(x))
#plt.title("A Sine Curve")
#plt.xlabel("x")
#plt.ylabel("sin(x)");
##The position, size, and style of these labels can
##be adjusted using optional arguments to the function. For more information, see the Matplotlib documentation and the docstrings of each of these functions.
##When multiple lines are being shown within a single
##axes, it can be useful to create a plot legend that
##labels each line type. Again, Matplotlib has a
##built-in way of quickly creating such a legend. It
##is done via the (you guessed it) plt.legend() method. Though there are several valid ways of using this, I find it easiest to specify the label of each line using the label keyword of the plot function:
#plt.plot(x, np.sin(x), '-g', label='sin(x)')
#plt.plot(x, np.cos(x), ':b', label='cos(x)')
#plt.axis('equal')
#plt.legend();

#For transitioning between MATLAB-style functions and
#object-oriented methods, make the following changes:
#plt.xlabel() → ax.set_xlabel()
#plt.ylabel() → ax.set_ylabel()
#plt.xlim() → ax.set_xlim()
#plt.ylim() → ax.set_ylim()
#plt.title() → ax.set_title()
##In the object-oriented interface to plotting, rather
##than calling these functions individually, it is
##often more convenient to use the ax.set() method to
##set all these properties at once:
#ax = plt.axes()
#ax.plot(x, np.sin(x))
#ax.set(xlim=(0, 10), ylim=(-2, 2),
#       xlabel='x', ylabel='sin(x)',
#       title='A Simple Plot');

##The command plt.subplots_adjust can be used to adjust
##the spacing between these plots.
#fig = plt.figure()
#fig.subplots_adjust(hspace=0.4, wspace=0.4)
#for i in range(1, 7):
#    ax = fig.add_subplot(2, 3, i)
#    ax.text(0.5, 0.5, str((2, 3, i)),
#           fontsize=18, ha='center')
##The hspace and wspace arguments of plt.subplots_adjust
##specify the spacing along the height and width of the
##figure, in units of the subplot size

#fig, ax = plt.subplots(2, 3, sharex='col', sharey='row')
##Note that by specifying sharex and sharey, we've
##automatically removed inner labels on the grid to make
##the plot cleaner. The resulting grid of axes instances
##is returned within a NumPy array, allowing for
##convenient specification of the desired axes using
##standard array indexing notation:
## axes are in a two-dimensional array, indexed by [row, col]
#for i in range(2):
#    for j in range(3):
#        ax[i, j].text(0.5, 0.5, str((i, j)),
#                      fontsize=18, ha='center')
#fig

##To go beyond a regular grid to subplots that span
##multiple rows and columns, plt.GridSpec() is the best
##tool. The plt.GridSpec() object does not create a plot
##by itself; it is simply a convenient interface that is
##recognized by the plt.subplot() command.
#grid = plt.GridSpec(2, 3, wspace=0.4, hspace=0.3)
#From this we can specify subplot locations and extents using the familiary Python slicing syntax:
#plt.subplot(grid[0, 0])
#plt.subplot(grid[0, 1:])
#plt.subplot(grid[1, :2])
#plt.subplot(grid[1, 2])
##########################################################

#fig, ((ax1),(ax2),(ax3),(ax4),(ax5),(ax6)) = plt.subplots(nrows=6,ncols=1, sharex=True)
#
#Cycle500 = file.CycleDataSet("2018-05-07 004334.txt", 292)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1,4.3),ax=ax1,label='Bhutan')
#ax1.set_title("Cycle 292")
#
#Cycle500 = file.CycleDataSet("2018-05-29 185432.txt", 360)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,20),ax=ax2,label='Bhutan')
#ax2.set_title("Cycle 360")
#
#Cycle500 = file.CycleDataSet("2018-06-05 120308.txt", 390)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,20),ax=ax3,label='Bhutan')
#ax3.set_title("Cycle 390")
#
#Cycle500 = file.CycleDataSet("2018-06-19 091637.txt", 429)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(0,5.5),ax=ax4,label='Bhutan')
#ax4.set_title("Cycle 429")
#
#Cycle500 = file.CycleDataSet("2018-07-03 093711.txt", 461)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2.75,6),ax=ax5,label='Bhutan')
#ax5.set_title("Cycle 461")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 500)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(218, 220))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2.5,8),ax=ax6,label='Bhutan')
#ax6.set_title("Cycle 500")
#
#fig.suptitle("Resistor ",fontsize='large')


####################

#fig, ((ax1),(ax2),(ax3),(ax4),(ax5),(ax6)) = plt.subplots(nrows=6,ncols=1, sharex=True)
#
#Cycle500 = file.CycleDataSet("2018-07-03 093711.txt", 461)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2.75,6),ax=ax1,label='Bhutan')
#ax1.set_title("Cycle 461")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 485)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,15),ax=ax2,label='Bhutan')
#ax2.set_title("Cycle 360")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 490)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(1.75,25),ax=ax3,label='Bhutan')
#ax3.set_title("Cycle 390")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 494)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2,25),ax=ax4,label='Bhutan')
#ax4.set_title("Cycle 429")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 498)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2.75,12),ax=ax5,label='Bhutan')
#ax5.set_title("Cycle 461")
#
#Cycle500 = file.CycleDataSet("2018-07-17 094742.txt", 500)
#df = Cycle500.cycleDataFrame
#Info500 = Cycle500.cycleInfo
#cycleTime = list(Info500["CorrectedTimes"])
#tempList = list(range(167, 169))
#for item in tempList:
#    df.iloc[:,item].plot(x=cycleTime,y=df.iloc[:,item],ylim=(2.5,20),ax=ax6,label='Bhutan')
#ax6.set_title("Cycle 500")
#
#fig.suptitle("Resistor 149",fontsize='large')






##################################
##################################
##################################
# Some simple generators can be coded succinctly as expressions using a syntax similar to list comprehensions but with parentheses instead of square brackets. These expressions are designed for situations where the generator is used right away by an enclosing function. Generator expressions are more compact but less versatile than full generator definitions and tend to be more memory friendly than equivalent list comprehensions.
#
# Examples:
# >>>
#
# >>> sum(i*i for i in range(10))                 # sum of squares
# 285
#
# >>> xvec = [10, 20, 30]
# >>> yvec = [7, 5, 3]
# >>> sum(x*y for x,y in zip(xvec, yvec))         # dot product
# 260
#
# >>> from math import pi, sin
# >>> sine_table = {x: sin(x*pi/180) for x in range(0, 91)}
#
# >>> unique_words = set(word  for line in page  for word in line.split())
#
# >>> valedictorian = max((student.gpa, student.name) for student in graduates)
#
# >>> data = 'golf'
# >>> list(data[i] for i in range(len(data)-1, -1, -1))
# ['f', 'l', 'o', 'g']
#
#################################
#################################
#################################



